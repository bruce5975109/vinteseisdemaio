#!/bin/bash

a=$1

if [ -e $a ]; then
	echo "ok"
else
	echo "erro!"
fi

echo "digite 1 para as 10 primeiras linhas"
echo "digite 2 para as 10 ultimas linhas"
echo "digite 3 para o numero de linha"
read op

if [ $op = "1" ]; then
	head -10 $1
elif [ $op = "2" ]; then
	tail - 10 $1
elif [ $op = "3" ]; then
	wc -l $1
else
	echo "erro!"
fi
